import 'package:flutter/material.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      title: "Leaning About Different notifications",
      home: Home(),
    );
  }
}

class Home extends StatefulWidget {
  @override
  _HomeState createState() => _HomeState();
}

class _HomeState extends State<Home> {
  //making the bottomsheet function:
  void showButton() {
    showModalBottomSheet<void>(
      context: context,
      builder: (BuildContext context) {
        return new Container(
          color: Colors.blueAccent,
          padding: EdgeInsets.all(32.0),
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              Text("Some Info Here"),
              RaisedButton(
                onPressed: () {
                  Navigator.pop(context);
                },
                child: Text("Close"),
                color: Colors.blue,
              ),
            ],
          ),
        );
      },
    );
  }

  //for the input alert box
  TextEditingController _textFieldController = TextEditingController();
  _displayDialog(BuildContext context) async {
    return showDialog(
        context: context,
        builder: (context) {
          return AlertDialog(
            title: Text('TextField AlertDemo'),
            content: TextField(
              controller: _textFieldController,
              decoration: InputDecoration(hintText: "TextField in Dialog"),
            ),
            actions: <Widget>[
              new FlatButton(
                child: new Text('SUBMIT'),
                onPressed: () {
                  Navigator.of(context).pop();
                },
              ),
            ],
          );
        });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      //use to control the overflow of the pixel
      resizeToAvoidBottomInset: false,
      backgroundColor: Colors.redAccent,
      appBar: AppBar(
        title: Text("Notification"),
      ),
      body: Container(
        padding: EdgeInsets.all(32.0),
        child: Center(
          child: Column(
            mainAxisAlignment: MainAxisAlignment.spaceEvenly,
            children: [
              //making the bottomsheet
              ElevatedButton(
                onPressed: () => showButton(),
                child: Text("Click Here For BottonSheet"),
              ),
              //making the snackbar
              ElevatedButton(
                onPressed: () {
                  final snackBar = SnackBar(
                    content: Text("Here is the text"),
                    action: SnackBarAction(
                      label: "Undo",
                      onPressed: () {},
                    ),
                  );
                  ScaffoldMessenger.of(context).showSnackBar(snackBar);
                },
                child: Text('Show SnackBar'),
              ),
              //here making the basic alert dialog box
              RaisedButton(
                color: Colors.deepPurple,
                onPressed: () {
                  return showDialog(
                    context: context,
                    builder: (ctx) => AlertDialog(
                      title: Text("Alert Dialog Box"),
                      content: Text("You have raised a Alert Dialog Box"),
                      actions: <Widget>[
                        FlatButton(
                          color: Colors.brown,
                          onPressed: () {
                            Navigator.of(ctx).pop();
                          },
                          child: Text("okay"),
                        ),
                      ],
                    ),
                  );
                },
                child: Text(
                  "Show alert Dialog box",
                ),
              ),
              //making the input alert box
              FlatButton(
                child: Text(
                  'Show Alert For Input Field',
                  style: TextStyle(fontSize: 20.0),
                ),
                padding: EdgeInsets.fromLTRB(20.0, 12.0, 20.0, 12.0),
                shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.circular(8.0)),
                color: Colors.green,
                onPressed: () => _displayDialog(context),
              ),
              //making the confirmation alert dialog box
              RaisedButton(
                onPressed: () async {
                  final ConfirmAction action =
                      await _asyncConfirmDialog(context);
                  print("Confirm Action $action");
                },
                child: const Text(
                  "Show Alert For Confirmation",
                  style: TextStyle(fontSize: 20.0),
                ),
                padding: EdgeInsets.fromLTRB(30.0, 10.0, 30.0, 10.0),
                shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.circular(8.0)),
                color: Colors.red,
              ),
              //product Selection confirmation  box
              RaisedButton(
                onPressed: () async {
                  final Product prodName = await _asyncSimpleDialog(context);
                  print("Selected Product is $prodName");
                },
                child: const Text(
                  "Alert For Product Selection",
                  style: TextStyle(fontSize: 20.0),
                ),
                padding: EdgeInsets.fromLTRB(30.0, 10.0, 30.0, 10.0),
                shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.circular(8.0)),
                color: Colors.green,
              ),
            ],
          ),
        ),
      ),
    );
  }
}

//For the confirmation alert
enum ConfirmAction { Cancel, Accept }
Future<ConfirmAction> _asyncConfirmDialog(BuildContext context) async {
  return showDialog<ConfirmAction>(
    context: context,
    barrierDismissible: false, // user must tap button for close dialog!
    builder: (BuildContext context) {
      return AlertDialog(
        title: Text('Delete This Contact?'),
        content: const Text('This will delete the contact from your device.'),
        actions: <Widget>[
          FlatButton(
            child: const Text('Cancel'),
            onPressed: () {
              Navigator.of(context).pop(ConfirmAction.Cancel);
            },
          ),
          FlatButton(
            child: const Text('Delete'),
            onPressed: () {
              Navigator.of(context).pop(ConfirmAction.Accept);
            },
          )
        ],
      );
    },
  );
}

//For the product Selection Dialog Box
enum Product { Apple, Samsung, Oppo, Redmi }

Future<Product> _asyncSimpleDialog(BuildContext context) async {
  return await showDialog<Product>(
      context: context,
      barrierDismissible: true,
      builder: (BuildContext context) {
        return SimpleDialog(
          title: const Text('Select Product '),
          children: <Widget>[
            SimpleDialogOption(
              onPressed: () {
                Navigator.pop(context, Product.Apple);
              },
              child: const Text('Apple'),
            ),
            SimpleDialogOption(
              onPressed: () {
                Navigator.pop(context, Product.Samsung);
              },
              child: const Text('Samsung'),
            ),
            SimpleDialogOption(
              onPressed: () {
                Navigator.pop(context, Product.Oppo);
              },
              child: const Text('Oppo'),
            ),
            SimpleDialogOption(
              onPressed: () {
                Navigator.pop(context, Product.Redmi);
              },
              child: const Text('Redmi'),
            ),
          ],
        );
      });
}
